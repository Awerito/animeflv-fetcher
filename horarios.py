import re
import httpx
import pandas as pd
from pprint import pprint
from fuzzywuzzy import process
from imgkit import from_string


SUCCESS = 200
URL = "http://horarios.ulagos.cl/2do%20semestre%202021_years_days_horizontal.html"
UTF_8 = '<meta charset="utf-8">\n'
VALUES = {
    "i": 1,
    "v": 5,
    "x": 10,
}

EMPTY_STR = str()


def roman_to_decimal(r):

    total = 0
    for i in range(len(r) - 1):
        left, right = r[i], r[i + 1]
        if VALUES[left] < VALUES[right]:
            total -= VALUES[left]
        else:
            total += VALUES[left]
    total += VALUES[r[-1]]
    return str(total)


def download_page(page=URL):

    LIST = 'Año <a href="(.*?)">\d*(.*?)<\/a>'
    ROMAN = "(?<!\w)[xvi]+(?:(?!n|s|i|o|a))"
    q = httpx.get(URL)
    if q.status_code == SUCCESS:
        data = dict()
        schedules_list = re.findall(LIST, q.text)
        for field in schedules_list:
            subject = (
                field[1]
                .lower()
                .replace("-", EMPTY_STR)
                .replace(":", EMPTY_STR)
                .replace("(", EMPTY_STR)
                .replace(")", EMPTY_STR)
                .replace(" nivel", EMPTY_STR)
                .replace("  ", " ")
                .replace("  ", " ")
                .removeprefix(" ")
                .removesuffix(" ")
            )

            roman = re.findall(ROMAN, subject)[0]
            subject = re.sub(ROMAN, roman_to_decimal(roman), subject, 1)

            try:
                roman = re.findall(ROMAN, subject)[0]
                subject = re.sub(ROMAN, roman_to_decimal(roman), subject, 1)
            except IndexError:
                pass

            data[subject] = field[0]
        return data, q.text
    return None


def fetch_schedule(search, data, page):

    finder_results = process.extract(search, data.keys(), limit=1)[0][0]
    table_id = data[finder_results][1:]
    search_regex = f'(<table id="{table_id}"(?:.|\n)*?<\/table>)'
    return re.findall(search_regex, page)[0]


def parse_html(html):

    HEADER = '\s*<tr><td rowspan="2"></td><th colspan="6">.*?</th></tr>'
    FOOTER = '(\s*<tr class="foot"><td><\/td><td colspan="6">(?:.*?)<\/td><\/tr>)'
    EMPTY_DAY = '<tr>\n.*?<th class="yAxis">(?:.*?)[^?  ALMUERZO]<\/th>\n.*?(?:(?:<td><\/td>\n).*){6}<\/tr>\n'
    EMPTY_BLOCK = '(\s*<tr>\n\s*<th class="yAxis">.*[^ALMUERZO]<\/th>(\s*<td></td>\n){6}\s*</tr>\n)'
    parsed_table = (
        html.replace("<!-- span -->", '<th class="xAxis">Horas</th>', 1)
        .replace("---", EMPTY_STR)
        .replace("-x-", EMPTY_STR)
        .replace("-X-", EMPTY_STR)
        .replace("<br />", "<br/>")
    )

    # Remove header, footer and empty blocks
    parsed_table = re.sub(EMPTY_BLOCK, EMPTY_STR, parsed_table)
    parsed_table = re.sub(HEADER, EMPTY_STR, parsed_table)
    parsed_table = re.sub(FOOTER, EMPTY_STR, parsed_table)
    parsed_table = parsed_table

    document = pd.read_html(parsed_table, index_col=0)
    document = document[0]
    document = document.dropna(axis=1, how="all")

    # TODO: keep rowspan
    parsed_table = document.to_html(na_rep=EMPTY_STR, justify="center")

    return parsed_table


if __name__ == "__main__":
    from sys import argv

    data, page = download_page()

    searches = argv[1:]

    html = str()
    for search in searches:
        aux = fetch_schedule(search, data, page)
        html = html + parse_html(aux)

    ok = from_string(UTF_8 + html, "out.jpeg", options={"quiet": ""})
